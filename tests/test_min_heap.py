"""
Tests to check if `Heap` functions as a proper min heap
"""

from dataclasses import dataclass
from typing import List

import pytest

from pylookupheap import Heap


def test_minheap_inits_empty():
    heap = Heap()
    assert len(heap) == 0


def test_minheap_can_init_with_empty_list():
    heap = Heap([])
    assert len(heap) == 0


def test_minheap_can_init_with_list():
    heap = Heap([3, 9, 5, 7])
    assert len(heap) == 4


def test_can_peak_top_of_minheap_without_removing():
    heap = Heap([3])
    assert len(heap) == 1
    assert heap.peek() == 3
    assert len(heap) == 1


def test_peeking_empty_minheap_returns_none():
    heap = Heap()
    assert heap.peek() is None


@pytest.mark.parametrize('arbitrary_list', [
    [7, 3, 11, 5, 9],
    [7, 3],
    [3]
])
def test_minheap_maintains_invariant_when_init_with_arbitrary_list(arbitrary_list: List):
    heap = Heap(arbitrary_list)
    assert heap.peek() == 3


@pytest.mark.parametrize('sorted_list', [
    [3, 5, 7],
    [3, 7],
    [3]
])
def test_minheap_maintains_invariant_when_init_with_sorted_list(sorted_list: List):
    heap = Heap(sorted_list)
    assert heap.peek() == 3


def test_can_push_elements_onto_minheap_while_maintaining_invariant():
    heap = Heap([7, 3, 11, 5])
    assert heap.peek() == 3
    heap.push(2)
    assert heap.peek() == 2
    heap.push(6)
    assert heap.peek() == 2


def test_minheap_size_properly_updated_when_pushing_elements():
    heap = Heap()
    assert len(heap) == 0
    heap.push(1)
    assert len(heap) == 1
    heap.push(2)
    assert len(heap) == 2


def test_can_pop_elements_from_minheap_while_maintaining_invariant():
    heap = Heap([3, 5, 7])
    assert heap.peek() == 3
    heap.pop()
    assert heap.peek() == 5
    heap.pop()
    assert heap.peek() == 7
    heap.pop()
    assert heap.peek() is None


def test_popping_from_empty_minheap_returns_none():
    heap = Heap()
    assert heap.pop() is None


def test_minheap_size_properly_updated_when_popping_element():
    heap = Heap([3, 5, 7])
    assert len(heap) == 3
    heap.pop()
    assert len(heap) == 2
    heap.pop()
    assert len(heap) == 1
    heap.pop()
    assert len(heap) == 0


def test_minheap_works_with_floating_point_values():
    heap = Heap([2.4, 5.5, 1.2, 1.8, 2.55])
    assert heap.peek() == 1.2
    heap.pop()
    assert heap.peek() == 1.8
    heap.push(0.05)
    assert heap.peek() == 0.05
    heap.push(0.06)
    assert heap.peek() == 0.05
    heap.pop()
    heap.pop()
    heap.pop()
    heap.pop()
    heap.pop()
    assert heap.peek() == 5.5


def test_minheap_works_with_string_keys():
    heap = Heap(['d', 'a', 'e', 'b', 'c'])
    assert heap.peek() == 'a'
    heap.push('aa')
    assert heap.peek() == 'a'
    heap.pop()
    assert heap.peek() == 'aa'
    heap.push('aaa')
    assert heap.peek() == 'aa'
    heap.pop()
    assert heap.peek() == 'aaa'
    heap.pop()
    heap.pop()
    heap.pop()
    heap.pop()
    assert heap.peek() == 'e'


def test_minheap_works_with_composite_types():
    """
    This test uses tuples to represent a composite type. The key of relevance is
    assumed to be at index 1 in this scenario.
    """
    heap = Heap([(7, 2, 4), (5, 1, 6), (9, 5, 4)], key=lambda x: x[1])
    assert heap.peek() == (5, 1, 6)
    heap.pop()
    assert heap.peek() == (7, 2, 4)
    heap.push((8, 0, 2))
    assert heap.peek() == (8, 0, 2)
    heap.pop()
    heap.pop()
    assert heap.peek() == (9, 5, 4)


def test_minheap_works_with_user_defined_classes():
    @dataclass
    class Task:
        priority: int
        desc: str = ""

    heap = Heap([Task(5), Task(0), Task(3)], key=lambda x: x.priority)
    assert heap.peek() == Task(0)
    heap.pop()
    assert heap.peek() == Task(3)
    heap.push(Task(2))
    assert heap.peek() == Task(2)
    heap.pop()
    heap.pop()
    assert heap.peek() == Task(5)
