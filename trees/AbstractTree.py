class Tree:
    """Abstract base class representing a Tree structure"""

    # Nested Position class
    class Position:
        """An abstraction representing the position of a single element"""
        def element(self):
            """Return the element stored at this position"""
            raise NotImplementedError("must be implemented by subclass")

        def __eq__(self, other):
            """Return True if Position represents the same location"""
            raise NotImplementedError("must be implemented by subclass")

        def __ne__(self, other):
            """Return True if other does not represent the same location"""
            return not (self == other)

    # abstract methods that subclass must implement
    def root(self):
        """Return Position representing tree's root elem or None if empty"""
        raise NotImplementedError("must be implemented by subclass")

    def parent(self, p):
        """Return Position representing p's parent or None if p is root"""
        raise NotImplementedError("must be implemented by subclass")

    def num_children(self, p):
        """Return integer number of children Position p has"""
        raise NotImplementedError("must be implemented by subclass")

    def children(self, p):
        """Generate an iteration of Positions representing p's children"""
        raise NotImplementedError("must be implemented by subclass")

    def __len__(self):
        """Return the total number of elements in the Tree"""
        raise NotImplementedError("must be implemented by subclass")

    # concrete methods for the Tree data structure
    def is_root(self, p):
        """Return True if Position p is the tree's root"""
        return self.root() == p

    def is_leaf(self, p):
        """Return true if Position p is a leaf node"""
        return self.num_children(p) == 0

    def is_empty(self):
        """Return True if tree is empty"""
        return len(self) == 0

    def depth(self, p):
        """Return the number of levels Position p is from the root"""
        if self.is_root():
            return 0
        else:
            return 1 + self.depth(self.parent(p))
