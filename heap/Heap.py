"""
Contains class that represents an encapsulated heap.
"""

import copy
from typing import Optional, MutableMapping


class Heap:
    """
    An encapsulated heap.
    """
    def __init__(self):
        self.key_map = {}
        self.lookup = {}

    def _key(self, idx):
        return self.key_map[self.heap[idx]]

    def __len__(self):
        return len(self.heap)

    def peek(self):
        if len(self.heap) == 0:
            return None
        else:
            return self.heap[0]

    def push(self, data, key):
        self.lookup[data]
        self.heap.append(data)
        self._sift_up(len(self.heap) - 1)

    def pop(self):
        if len(self.heap) == 0:
            return
        removed = self.heap.pop()
        if len(self.heap):
            self.heap[0] = removed
            self._sift_down(0)

    def _sift_up(self, idx):
        p = self._parent_idx(idx)
        while p is not None and self._key(p) > self._key(idx):
            self._swap(p, idx)
            idx = p
            p = self._parent_idx(idx)

    def _sift_down(self, idx):
        c = self._child_to_swap_idx(idx)
        while c is not None and self._key(c) < self._key(idx):
            self._swap(c, idx)
            idx = c
            c = self._child_to_swap_idx(idx)

    def _swap(self, x, y):
        temp = self.heap[x]
        self.heap[x] = self.heap[y]
        self.heap[y] = temp

    def _parent_idx(self, idx):
        if 0 < idx < len(self.heap):
            return (idx - 1) // 2
        else:
            return None

    def _child_to_swap_idx(self, idx):
        lc_idx = idx * 2 + 1
        rc_idx = idx * 2 + 2

        lc = self._key(lc_idx)
        rc = self._key(rc_idx)

        if rc is None and lc is None:
            return None
        else:
            if lc is not None:
                return lc_idx if rc is None or lc < rc else rc_idx
            else:
                return rc_idx
