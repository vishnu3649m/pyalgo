Data structures & containers for Python
---------------------------------------

Implementations of data structures not provided by the standard library and better abstracted data structures (e.g. Heap).

