# Implementation of a stack ADT using python's list data structure


class Empty(Exception):
    """Error when attempting to access an element from an empty container"""
    pass


class ArrayStack:
    """Stack, a LIFO data structure implemented using a list container"""

    def __init__(self):
        """Initializing the data structure with a list of a certain capacity"""
        self

    def __len__(self):
        """built-in operator that gives the num items in stack"""
        pass

    def push(self, elem):
        """Inserts elem to top of the stack"""
        pass

    def pop(self):
        """Removes and returns the element at the top of stack"""
        pass

    def top(self):
        """returns the elem which is on top of the stack without removing it"""
        pass

    def is_empty(self):
        """return True if no elements in stack"""
        pass

    def debug_print(self):
        """prints out the current contents of the stack"""
